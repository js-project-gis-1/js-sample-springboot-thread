package com.sp.service;

import com.sp.model.Hero;
import com.sp.repository.HeroRepository;

public class DisplayRunnable implements Runnable {

	private HeroRepository hrepo;
	boolean isEnd = false;

	public DisplayRunnable(HeroRepository hrepo) {
		this.hrepo = hrepo;
	}

	@Override
	public void run() {
		while (!this.isEnd) {
			try {
				Thread.sleep(10000);
				for (Hero h : this.hrepo.findAll()) {
					System.out.println(h.toString());
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("Runnable DisplayRunnable ends.... ");
	}

	public void stop() {
		this.isEnd = true;
	}

}
